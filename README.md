# ftp-master fancy NEW

A service that collects **publicly available information** from different sources and displays the Debian **NEW** queue in a fancy format.

Also useful metrics can be extracted such as the number of yearly attempted uploads (= ACCEPTS + REJECTS) versus the REJECTS; for example this is the result for the **pkg-javascript-devel** team:

![js-team metrics](https://salsa.debian.org/paolog-guest/ftp_master_fancy_new/raw/master/js-team_stats.png)

This project is a **proof of concept**.

The results should be taken with a grain of salts; use the metrics for good not for evil.

For **authoritative** data, always refer to the [official list](https://ftp-master.debian.org/new.html) !

## Mechanics

The implementation is a hack; at a later stage if it proves to be useful we should set up proper APIs with ftpmaster.

ATM there are three components:

1. a [python3 script](https://salsa.debian.org/paolog-guest/ftp_master_fancy_new/blob/master/scripts/parse_queue.py) that parses the ftp-master NEW queue in RFC 822 format and scraps the version HTML pages, and stores records in mongo (`new` collection); packages that disappear from the NEW queue (either ACCEPTED or REJECTED) are not deleted: they are just marked as inactive

2. a [python3 script](https://salsa.debian.org/paolog-guest/ftp_master_fancy_new/blob/master/scripts/parse_mailing_lists.py) that parses the team mailing lists and retrieves ACCEPTED and REJECTED messages, and stores records in mongo (`accepted` and `rejected` collections)
ATM only the [mailing lists on alioth.d.n](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo) are parsed.
ACCEPTs of NEW packages can be euristically discriminated from updates of packages already in the archive matching the `changes` field of the `...changes` file with the case-insensitive regex `/initial (packaging|release|upload)/`.

3. an [HTML view](https://salsa.debian.org/paolog-guest/ftp_master_fancy_new/blob/master/www/index.html) based on jQuery DataTables that shows the NEW queue in a fancy format.

## Info

Source code repository: [https://salsa.debian.org/paolog-guest/ftp_master_fancy_new](https://salsa.debian.org/paolog-guest/ftp_master_fancy_new)

Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>

Licence: GPL-3+

## TODO

- add timeouts and retry to the scraping scripts

- Use libjs-jquery, libjs-jquery-datatables, libjs-jquery-datatables-extensions rather than local copies and configure nginx to serve them (see [#818585](https://bugs.debian.org/818585)):

```
location /javascript/ {
  alias /usr/share/javascript/;
}
```

- parse all [mailing lists on d.o](https://lists.debian.org/devel.html):

  - [ ] debian-blends@lists.debian.org
  - [ ] debian-common-lisp@lists.debian.org
  - [ ] debian-fonts@lists.debian.org
  - [ ] debian-hpc@lists.debian.org
  - [ ] debian-kernel@lists.debian.org
  - [ ] debian-ocaml-maint@lists.debian.org
  - [ ] debian-openoffice@lists.debian.org
  - [ ] debian-qt-kde@lists.debian.org
  - [ ] debian-x@lists.debian.org

  **on hold**, see:

  - https://lists.debian.org/debian-devel/2005/04/msg01053.html
  - https://bugs.debian.org/161440

- run `sloccount` on the repos where available to have an estimate of the complexity of the packages

- have separate pages with stats for:

  - sources
  - uploaders
  - sponsors
  - and teams

## Install

```
sudo apt install nginx mongodb python3-pymongo libjs-jquery-datatables libjs-jquery-datatables-extensions nginx python3-debian mongo-tools mongodb python3-bs4 python3-aiohttp
```

## Mongo setup and maintenance

To initialize mongodb:
```
mongo ftp_master
db.createCollection('new')
db.createCollection('accepted')
db.createCollection('rejected')
```

Reset database:
```
mongo ftp_master
db.new.drop()
db.accepted.drop()
db.rejected.drop()
db.createCollection('new')
db.createCollection('accepted')
db.createCollection('rejected')
```

To browse mongodb data:
```
mongo ftp_master
show collections
// count all records
db.new.find().count()
// count inactive records
db.new.find({"active": false}).count()
// list records without git repo link
db.new.find({vcs_git: null}, {source:1, _id:0})
// list distinct maintainers
db.new.distinct('maintainer')
// aggregate rejected uploads by month-year:
db.rejected.aggregate({$match: {'list_name': 'pkg-javascript-devel'}}, {$group: { _id: { month: { $month: "$date"}, year: { $year: "$date"}  }, count: { $sum: 1} } })
// aggregate rejected uploads by year:
db.rejected.aggregate({$match: {'list_name': 'pkg-javascript-devel'}}, {$group: { _id: { year: { $year: "$date"}  }, count: { $sum: 1} } })
// aggregate accepted initial packaging / upload / release by year:
db.accepted.find({'list_name': 'pkg-javascript-devel'}).count()
3864
db.accepted.find({$and: [{'list_name': 'pkg-javascript-devel'}, {'changes.changes': /initial (packaging|release|upload)/i}]}).count()
1269
db.accepted.aggregate({$match: {$and: [{'list_name': 'pkg-javascript-devel'}, {'changes.changes': /initial (packaging|release|upload)/i}]}}, {$group: { _id: { year: { $year: "$date"}  }, count: { $sum: 1} } })

// count active records
db.new.find({'active': true}).count()
// find accepted package by dsc md5sum:
db.accepted.findOne({"changes.files.md5sum": "5b1b8a8a708d5b815a184aed76bacb9a"}).changes.files[0]
// find rejected package versions by source name:
db.rejected.find({"source": "node-lodash"}, {version: 1})
// find ACCEPTED/REJECTED counts per team
db.rejected.find({'list_name': 'debian-med-packaging'}).count()
db.rejected.find({'list_name': 'pkg-javascript-devel'}).count()
db.accepted.find({'list_name': 'debian-med-packaging'}).count()
db.accepted.find({'list_name': 'pkg-javascript-devel'}).count()

// accepts (only initial uploads) and rejects grouped by team
db.accepted.aggregate({$match: {'changes.changes': /initial (packaging|release|upload)/i}},  {"$group" : {_id: "$changes.maintainer", count: {$sum: 1}}})
db.rejected.aggregate({"$group" : {_id: "$list_name", count: {$sum: 1}}})

// packages from a certain team with a certain standards_version:
db.new.find({   $and: [     {'maintainer': 'pkg-javascript-devel@lists.alioth.debian.org'},     {'dsc.standards_version': '4.1.3'}   ] }, {source:1})
```

To refresh the `www/new.json` file:
```
mongo ftp_master --quiet --eval 'db.new.find({}, {id: 1, source: 1, actual_version: 1, active: 1, maintainer: 1, changed_by: 1, sponsored_by: 1, last_modified: 1, age: 1, dsc: 1, changes: 1, _id:0}).toArray()' > www/new.json 
```

Backup:
```
mongodump --db ftp_master
```

Restore:
```
mongorestore dump/
```

## Test locally

```
chromium --allow-file-access-from-files www/index.html 
```

## Publish

```
./scripts/update.sh
```

## Database schemas

The schemas for the three collections are:

### new

the primary key is the `id` field which contains the string `source@01234567890123456789012345678012` composed of the source name and the **md5** sum of the submitted dsc
```
{
  "_id" : ObjectId("5aec285997bc7565dec9158c"),
  "source" : "node-npm-packlist",
  "binary" : "node-npm-packlist",
  "version" : "1.1.10-1",
  "architectures" : "source, all",
  "age" : "21 hours",
  "last_modified" : "1525346421",
  "queue" : "new",
  "maintainer" : "pkg-javascript-devel@lists.alioth.debian.org",
  "changed_by" : "sumedh.patkar19@gmail.com",
  "sponsored_by" : "praveen@debian.org",
  "distribution" : "unstable",
  "fingerprint" : "2A7974AE2FC152D77867DA4ACE1F9C674512C22A",
  "closes" : "#895202",
  "changes_file" : "node-npm-packlist_1.1.10-1_amd64.changes",
  "url" : "https://ftp-master.debian.org/new/node-npm-packlist_1.1.10-1.html",
  "changes" : {
    "format" : "1.8",
    "date" : "Sun, 08 Apr 2018 10:41:11 +0000",
    "source" : "node-npm-packlist",
    "binary" : "node-npm-packlist",
    "architecture" : "source all",
    "version" : "1.1.10-1",
    "distribution" : "unstable",
    "urgency" : "low",
    "maintainer" : "pkg-javascript-devel@lists.alioth.debian.org",
    "changed_by" : "sumedh.patkar19@gmail.com",
    "description" : "node-npm-packlist - Get a list of the files to add from a folder into an npm package",
    "closes" : "895202",
    "changes" : "node-npm-packlist (1.1.10-1) unstable; urgency=low\n\n  * Initial release (Closes: #895202)",
    "files" : [
      {
        "md5sum" : "249a1c4208a4c4dc26833d5b6088d5aa",
        "size" : "2136",
        "section" : "javascript",
        "priority" : "optional",
        "name" : "node-npm-packlist_1.1.10-1.dsc",
        "extension" : "dsc"
      },
      {
        "md5sum" : "8584ceb06a63238820994c4bb2ed55c3",
        "size" : "19993",
        "section" : "javascript",
        "priority" : "optional",
        "name" : "node-npm-packlist_1.1.10.orig.tar.gz",
        "extension" : "gz"
      },
      {
        "md5sum" : "ce3bf520a4be333d371fe7379b95b72e",
        "size" : "2220",
        "section" : "javascript",
        "priority" : "optional",
        "name" : "node-npm-packlist_1.1.10-1.debian.tar.xz",
        "extension" : "xz"
      },
      {
        "md5sum" : "5229a43f61fe80a235e4c0045beab280",
        "size" : "6080",
        "section" : "javascript",
        "priority" : "optional",
        "name" : "node-npm-packlist_1.1.10-1_all.deb",
        "extension" : "deb"
      },
      {
        "md5sum" : "92ff3921aae1194890c1326a5b853bca",
        "size" : "7157",
        "section" : "javascript",
        "priority" : "optional",
        "name" : "node-npm-packlist_1.1.10-1_amd64.buildinfo",
        "extension" : "buildinfo"
      }
    ]
  },
  "dsc" : {
    "format" : "3.0 (quilt)",
    "source" : "node-npm-packlist",
    "binary" : "node-npm-packlist",
    "architecture" : "all",
    "version" : "1.1.10-1",
    "maintainer" : "pkg-javascript-devel@lists.alioth.debian.org",
    "uploaders" : "Sumedh Patkar <sumedh.patkar19@gmail.com>",
    "homepage" : "https://www.npmjs.com/package/npm-packlist",
    "standards_version" : "4.1.4",
    "vcs_browser" : "https://salsa.debian.org/js-team/node-npm-packlist",
    "vcs_git" : "https://salsa.debian.org/js-team/node-npm-packlist.git",
    "testsuite" : "autopkgtest",
    "testsuite_triggers" : "node-tap",
    "build_depends" : "debhelper (>=11), nodejs, node-tap, node-npm-bundled, node-ignore-walk",
    "package_list" : "node-npm-packlist deb javascript optional arch=all\n",
    "files" : [
      {
        "md5sum" : "8584ceb06a63238820994c4bb2ed55c3",
        "size" : "19993",
        "name" : "node-npm-packlist_1.1.10.orig.tar.gz",
        "extension" : "gz"
      },
      {
        "md5sum" : "ce3bf520a4be333d371fe7379b95b72e",
        "size" : "2220",
        "name" : "node-npm-packlist_1.1.10-1.debian.tar.xz",
        "extension" : "xz"
      }
    ]
  },
  "source_lintian" : "\n\n",
  "source_readmesource" : "\nNo README.source in this package\n\n\n",
  "ctime" : 1525426033,
  "mtime" : 1525428488,
  "active" : true,
  "id" : "node-npm-packlist@249a1c4208a4c4dc26833d5b6088d5aa"
}
```

### rejected

the primary key is the `message_id` field

```
{
  "_id" : ObjectId("5aec111c97bc755ed9df301f"),
  "list_name" : "pkg-javascript-devel",
  "message_id" : "<E1dmMXd-0001o7-2a@fasolo.debian.org>",
  "date" : ISODate("2017-08-28T18:05:33Z"),
  "from" : "ftpmaster@ftp-master.debian.org",
  "subject" : "[Pkg-javascript-devel] nodejs_6.11.2~dfsg-3_source.changes REJECTED",
  "body" : "\n\nSource-only uploads to NEW are not allowed.\n\nbinary:nodejs-doc is NEW.\n\n===\n\nPlease feel free to respond to this email if you don't understand why\nyour files were rejected, or if you upload new files which address our\nconcerns.\n\n\n", "source" : "nodejs", "version" : "6.11.2~dfsg-3"
}
```

### accepted

the primary key is the `message_id` field

```
{
  "_id" : ObjectId("5aec110e97bc755ed9df2c41"),
  "list_name" : "pkg-javascript-devel",
  "message_id" : "<E1c4ERC-000F27-45@fasolo.debian.org>",
  "date" : ISODate("2016-11-08T23:00:14Z"),
  "from" : "ftpmaster@ftp-master.debian.org",
  "subject" : "[Pkg-javascript-devel] node-fs-extra_1.0.0-1_amd64.changes ACCEPTED\tinto unstable, unstable",
  "source" : "node-fs-extra",
  "version" : "1.0.0-1",
  "changes" : {
    "format" : "1.8",
    "date" : "Sun, 06 Nov 2016 09:44:32 +0100",
    "source" : "node-fs-extra",
    "binary" : "node-fs-extra",
    "architecture" : "source all",
    "version" : "1.0.0-1",
    "distribution" : "unstable",
    "urgency" : "medium",
    "maintainer" : "pkg-javascript-devel@lists.alioth.debian.org",
    "changed_by" : "julien.puydt@laposte.net",
    "description" : "\n node-fs-extra - fs-extra contains methods not included in the Node.js fs module",
    "closes" : "843350",
    "changes" : "\n node-fs-extra (1.0.0-1) unstable; urgency=medium\n .\n   [ Bas Couwenberg ]\n   * Initial packaging.\n .\n   [ Julien Puydt ]\n   * Bump dh compat to 10.\n   * Bump standards-version to 3.9.8.\n   * Use https for Vcs-* fields.\n   * Add myself to uploaders.\n   * New upstream release. (Closes: #843350)",
    "checksums_sha1" : [
      {
        "sha1" : "62c604f3b87652c78e4c3e95e54048a669c167d0",
        "size" : "2000",
        "name" : "node-fs-extra_1.0.0-1.dsc"
      },
      {
        "sha1" : "fe55f9393fc6718fae3f122db57e7c709e5db65c",
        "size" : "51946",
        "name" : "node-fs-extra_1.0.0.orig.tar.gz"
      },
      {
        "sha1" : "ebcc08b6d1d573f340dd99bdf200a6fe97b1cc13",
        "size" : "2472",
        "name" : "node-fs-extra_1.0.0-1.debian.tar.xz"
      },
      {
        "sha1" : "310415aa52b1c3d525aa34dd2daa8d51c7b14f60",
        "size" : "4501",
        "name" : "node-fs-extra_1.0.0-1_20161108T164203z-67dbf797.buildinfo"
      },
      {
        "sha1" : "eefe73750b45eb07140347520e6eefd7b837731d",
        "size" : "49638",
        "name" : "node-fs-extra_1.0.0-1_all.deb"
      }
    ],
    "checksums_sha256" : [
      {
        "sha256" : "a4bd86ca5afcbafc74764041ed6e515e7a81c6e0bb4d232d9d49497860ea3de6",
        "size" : "2000",
        "name" : "node-fs-extra_1.0.0-1.dsc"
      },
      {
        "sha256" : "4190a227014c1ae07e5eba84139c7c3c5834054a16b5083e682333016a524d79",
        "size" : "51946",
        "name" : "node-fs-extra_1.0.0.orig.tar.gz"
      },
      {
        "sha256" : "e9339a03ed99840ea2b4978aff924bc1f7bc12d19acbdc913f0816e79c1bbb70",
        "size" : "2472",
        "name" : "node-fs-extra_1.0.0-1.debian.tar.xz"
      },
      {
        "sha256" : "eb4dbb693791d8e32b111f65629a16fe01d2fb884c05bdd7fda118f06d159f9f",
        "size" : "4501",
        "name" : "node-fs-extra_1.0.0-1_20161108T164203z-67dbf797.buildinfo"
      },
      {
        "sha256" : "60deaa9d3f662dd4049086d6ced26fbd202d3926bb37b2121727e45d57b75ce3",
        "size" : "49638",
        "name" : "node-fs-extra_1.0.0-1_all.deb"
      }
    ],
    "files" : [
      {
        "md5sum" : "380b6600316ded134a2453f5e809d81b",
        "size" : "2000",
        "section" : "web",
        "priority" : "extra",
        "name" : "node-fs-extra_1.0.0-1.dsc"
      },
      {
        "md5sum" : "eb5cd456cc563a8f0dc6c654065c013d",
        "size" : "51946",
        "section" : "web",
        "priority" : "extra",
        "name" : "node-fs-extra_1.0.0.orig.tar.gz"
      },
      {
        "md5sum" : "0cb6c67c6d4717c39792f89678d814c3",
        "size" : "2472",
        "section" : "web",
        "priority" : "extra",
        "name" : "node-fs-extra_1.0.0-1.debian.tar.xz"
      },
      {
        "md5sum" : "67dbf797ffcef1d6f905b575e255501c",
        "size" : "4501",
        "section" : "web",
        "priority" : "extra",
        "name" : "node-fs-extra_1.0.0-1_20161108T164203z-67dbf797.buildinfo"
      },
      {
        "md5sum" : "db55099476f1def90743ee3414de9414",
        "size" : "49638",
        "section" : "web",
        "priority" : "extra",
        "name" : "node-fs-extra_1.0.0-1_all.deb"
      }
    ]
  }
}
```
