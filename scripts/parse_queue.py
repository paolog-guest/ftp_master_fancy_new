#!/usr/bin/env python3
# coding=utf-8
#
# - parses the ftp-master NEW queue in RFC 822 format
# - asynchronously scrapes the matching version HTML pages
# - stores all data in mongobd
#
# This file is part of ftp_master_fancy_new
# Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup
import requests
import asyncio
import aiohttp
import pymongo
import time
from email.utils import parseaddr
import sys

time_format = "%Y-%m-%dT%H:%M:%SZ"


async def get_fields(client, source):
    url = source['url']
    async with client.get(url) as response:
        print('queueing %s' % url)
        html = await response.read()
        fields = {}
        soup = BeautifulSoup(html, 'lxml')
        tbodies = soup.find_all('tbody')
        for t in tbodies:
            if t['id'] == 'changes-body':
                changes = {}
                rows = t.find_all('tr')
                for row in rows:
                    cols = row.find_all('td')
                    key = cols[0].get_text().lower().replace('-', '_')[:-1]
                    value = cols[1].get_text()
                    if key == 'changed_by' or key == 'maintainer':
                        value = parseaddr(value.replace(' at ', '@'))[1]
                    if key == 'files':
                        files = []
                        for line in value.splitlines():
                            record = line.split(' ')
                            files.append({'md5sum': record[0], 'size': record[1], 'section': record[2], 'priority': record[3], 'name': record[4], 'extension': record[4].split('.')[-1]})
                        value = files
                    changes[key] = value
                fields['changes'] = changes
            if t['id'] == 'dsc-body':
                dsc = {}
                rows = t.find_all('tr')
                for row in rows:
                    cols = row.find_all('td')
                    key = cols[0].get_text().lower().replace('-', '_')[:-1]
                    value = cols[1].get_text()
                    if key == 'changed_by' or key == 'maintainer':
                        value = parseaddr(value.replace(' at ', '@'))[1]
                    if key == 'files':
                        files = []
                        for line in value.splitlines():
                            record = line.split(' ')
                            files.append({'md5sum': record[0], 'size': record[1], 'name': record[2], 'extension': record[2].split('.')[-1]})
                        value = files
                    dsc[key] = value
                fields['dsc'] = dsc
            if t['id'] == 'source-lintian-body':
                fields['source_lintian'] = t.get_text()
            if t['id'] == 'source-readmesource-body':
                fields['source_readmesource'] = t.get_text()
            # binary-.*-control-body
            # binary-.*-lintian-body
            # binary-.*-contents-body
            # binary-.*-copyright-body
        print('processed %s' % source['source'])
        source.update(fields)


async def main(loop, sources):
    async with aiohttp.ClientSession(loop=loop) as client:
        return await asyncio.gather(
            *[get_fields(client, source) for source in sources]
        )


print('%s - %s started' % (time.strftime(time_format, time.gmtime()), sys.argv[0]))

client = pymongo.MongoClient()
db = client.ftp_master
active_before = db.new.find({'active': True}).count()

new822 = requests.get('https://ftp-master.debian.org/new.822').text
lines = new822.splitlines()

sources = []
source = {}
for line in lines:
    if line != '':
        kv = line.split(':')
        key = kv[0].lower().replace('-', '_')
        value = ':'.join(kv[1:]).strip()
        source[key] = value
    else:
        sources.append(source)
        source = {}

timestamp = int(time.time())
new = []
maintainers = {}
uploaders = {}
sponsors = {}
count = 0
for source in sources:
    if source['queue'] == 'new':
        source['changed_by'] = parseaddr(source['changed_by'])[1]
        source['maintainer'] = parseaddr(source['maintainer'])[1]
        version = source['changes_file'].split('_')[1]
        if ':' in source['version']:
            version = "%s:%s" % (source['version'].split(':')[0], version)
        url = "https://ftp-master.debian.org/new/%s_%s.html" % (source['source'], version)
        source['url'] = url
        new.append(source)
    else:
        print('skipping %d/%d at %s' % (count, len(sources), source['source']))
    count += 1

loop = asyncio.get_event_loop()
loop.run_until_complete(main(loop, new))

count = 0
db.new.update_many({}, {"$set": {"mtime": timestamp, "active": False}})
for source in new:
    if 'changes' in source:
        source['ctime'] = timestamp
        source['mtime'] = timestamp
        source['active'] = True
        md5sum = '01234567890123456789012345678012'  # default value
        for f in source['changes']['files']:
            if f['extension'] == 'dsc':
                md5sum = f['md5sum']
        source['id'] = "%s@%s" % (source['source'], md5sum)
        print('looking at %s' % source['id'])
        found = db.new.find({'id': source['id']})
        if found.count() == 0:
            print('NEW %s' % source['source'])
            db.new.insert(source)
            count += 1
        else:
            db.new.update_one({'id': source['id']}, {"$set": {"active": True, "mtime": source['mtime']}})

active_after = db.new.find({'active': True}).count()

print('%s - added %d, deactivated %d' % (time.strftime(time_format, time.gmtime()), count, active_before + count - active_after))
